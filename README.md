# Randall Crandon's Personal Website &amp; Portfolio

This is the GitHub repository for my portfolio!

I'm a new Full Stack Software Engineer, and I've done my best to make my site accessible and easy to use. If you have any suggestions, feel free to open an issue or pull request :-)

See my portfolio at [rcrandon.com](https://rcrandon.gitlab.io).
